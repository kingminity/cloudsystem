package Cloud;
import java.net.ServerSocket;
import java.net.Socket;

import Client.ClientServer;
import Server.MinecraftServer;
import Server.ServerState;

public class Acceptor {
	private ServerSocket ss;

	private boolean cancel;

	public Acceptor(ServerSocket ss) {
		this.ss = ss;
		cancel = false;
	}

	public void start() {
		System.out.println("[CloudSystem] Server-Acceptor wird gestartet ...");
		Thread acceptorThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (!cancel) {
						Socket s = ss.accept();
						CListener cl = new CListener(s);
						cl.listen();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		acceptorThread.start();
		System.out.println("[CloudSystem] Server-Acceptor wurde erfolgreich gestartet");
	}

	public void closeAll() {
		cancel = true;
		for (MinecraftServer ms : Data.getMinecraftServer()) {
			if(! ms.getServerState().equals(ServerState.BUILD)){
				ms.getCListener().closeClient(ms.getCListener());
			}
		}
		for(ClientServer cs : Data.getClients()){
			cs.getCListener().closeClient(cs.getCListener());
		}
	}
	
}

package Cloud;

import java.util.ArrayList;
import java.util.HashMap;

import Client.ClientList;
import Server.BungeecordServer;
import Server.MinecraftServer;
import Server.ServerType;

public class Data {
	
	private static HashMap<ServerType, Integer> maxPlayerInServer = new HashMap<ServerType, Integer>();
	private static HashMap<ServerType, Integer> playerInServer = new HashMap<ServerType, Integer>();
	private static HashMap<ServerType, Integer> maxPlayerInServerLobbys = new HashMap<ServerType, Integer>();
	private static HashMap<ServerType, Integer> playerInServerLobbys = new HashMap<ServerType, Integer>();
	private static int onlinePlayer = 0;
	private static ArrayList<MinecraftServer> server = new ArrayList<MinecraftServer>();
	private static ClientList client = new ClientList();
	private static BungeecordServer bungeecord = null;
	
	public static ClientList getClients(){
		return client;
	}
	
	public static BungeecordServer getBungeeCord(){
		return bungeecord;
	}

	public static ArrayList<MinecraftServer> getMinecraftServer() {
		return server;
	}
	
	public static HashMap<ServerType, Integer> getMaxPlayerInServer(){
		return maxPlayerInServer;
	}
	
	public static HashMap<ServerType, Integer> getPlayerInServer(){
		return playerInServer;
	}
	
	public static HashMap<ServerType, Integer> getMaxPlayerInServerLobbys(){
		return maxPlayerInServerLobbys;
	}
	
	public static HashMap<ServerType, Integer> getPlayerInServerLobbys(){
		return playerInServerLobbys;
	}
	
	public static int getOnlinePlayer(){
		return onlinePlayer;
	}
	
	
	
	
	
	
	public static void setOnlinePlayer(int onlinePlayer){
		Data.onlinePlayer = onlinePlayer;
	}

	public static void setBungeeCord(BungeecordServer bungeecord) {
		Data.bungeecord	= bungeecord;
		
	}


}

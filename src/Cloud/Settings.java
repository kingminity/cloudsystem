package Cloud;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings{
	
	CloudSystem system;
	
	int port;
	
	public Settings(CloudSystem system){
		
		System.out.println("[CloudSystem] Settings werden ausgelesen ...");
		
		this.port = 0;
		
		File f = new File(new File("").getAbsolutePath(), "Settings.properties");
		if(! f.isFile()){
			System.out.println("[CloudSystem] Settings.properties wurde nicht gefunden");
			
			Properties serverdata = new Properties();
			serverdata.setProperty("Port", "0");
			try {
				FileOutputStream out = new FileOutputStream(f);
				serverdata.store(out, null);
				out.close();
				
				System.out.println("[CloudSystem] Neue Settings.properties wurde erstellt");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Properties p = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(f);
			p.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.port = Integer.parseInt(p.getProperty("Port"));
		
		
		if(this.port == 0){
			System.out.println("[CloudSystem] Settings sind ungültig");
			
			system.shutdown();
		} else {
			System.out.println("[CloudSystem] Settings wurden erfolgreich ausgelesen");
		}
	}
	
	public void reload(){
		System.out.println("[CloudSystem] Settings werden neu geladen ...");
		System.out.println("[CloudSystem] Die Client ID wird dabei nicht berücksichtigt");
		
		File f = new File(new File("").getAbsolutePath(), "Settings.properties");
		Properties p = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(f);
			p.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.port = Integer.parseInt(p.getProperty("Port"));
		
		
		if(this.port == 0){
			System.out.println("[CloudSystem] Settings sind ungültig");
			
			system.shutdown();
		} else {
			System.out.println("[CloudSystem] Settings wurden erfolgreich neu geladen");
		}
	}
	
	
	
	

}

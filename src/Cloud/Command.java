package Cloud;

import java.util.Iterator;

import Client.ClientServer;
import Server.LobbyServer;
import Server.MinecraftServer;
import Server.ServerState;
import Server.ServerType;

public class Command{

	public static void executeCommand(String data, CListener listener) {
		String[] cmd = data.split(" ");
		switch (cmd[0]) {
		case "SERVER":
			switch (cmd[1]) {
			case "REGISTER":
				if(cmd[2].equals("BUNGEECORD")){
					if(Data.getBungeeCord() != null){
						Data.getBungeeCord().setListener(listener);
						Data.getBungeeCord().setServerState(ServerState.RUN);
						System.out.println(Data.getBungeeCord().getServerState());
						System.out.println("[CloudSystem] BungeeCord-Server registriert");
					}
				} else {
					if(Data.getBungeeCord().getServerState().equals(ServerState.RUN)){
						if(cmd.length == 9){
							int id = Integer.parseInt(cmd[3]);
							MinecraftServer ms = null;
							Iterator it = Data.getMinecraftServer().iterator();
							while(it.hasNext()){
								MinecraftServer all = (MinecraftServer) it.next();
								if(all.getID() == id){
									ms = all;
									it.remove();
								}
							}
							//Servertype = cmd[2] Id = cmd[3] IP = cmd[4] Port = cmd[5] Servername = cmd[6] ServerState = cmd[7] Player = cmd[8]
							ms.setServerState(ServerState.valueOf(cmd[7]));
							ms.init(cmd[4], Integer.parseInt(cmd[5]),cmd[6], listener, Integer.parseInt(cmd[8]));
							Data.getMinecraftServer().add(ms);
							System.out.println("[CloudConnector] Neuer Server (ID: " + id + " | ServerType: " + cmd[2] + ") registriert");
							
							//BungeeCord
							Data.getBungeeCord().getListener().write("REGISTER " + ms.getName() + " " + ms.getIP() + " " + ms.getPort());
							
							if(ServerType.valueOf(cmd[2]).equals(ServerType.LOBBY)){
								for(MinecraftServer all : Data.getMinecraftServer()){
									if(! (all instanceof LobbyServer)){
										if(! all.getServerState().equals(ServerState.BUILD)){
											ms.getCListener().write("LOBBYSYSTEM REGISTER " + all.getName() + " " + all.getServerState() + " " + all.getMap().getName() + " " + all.getMap().getSize() + " " + all.getPlayer());
										}
									}
								}
							} else {
								for(MinecraftServer all : Data.getMinecraftServer()){
									if(all instanceof LobbyServer){
										if(! all.getServerState().equals(ServerState.BUILD)){
											all.getCListener().write("LOBBYSYSTEM REGISTER " + ms.getName() + " " + ms.getServerState() + " "  + ms.getMap().getName() + " " + ms.getMap().getSize() + " " + ms.getPlayer());
										}
									}
								}
							}
						}
			
					}
				}
				break;
			}
			break;
		case "JOINSYSTEM":
			MinecraftServer target;
			switch(cmd[1]){
			case "TELEPORT":
				//cmd[2] = player cmd[3] = servername 
				target = null;
				for(MinecraftServer server : Data.getMinecraftServer()){
					if(server.getName().equals(cmd[3])){
						target = server;
					}
				}
				if(target != null){
					if(target.getServerState() != ServerState.BUILD && target.getServerState() != ServerState.STOP && target.getServerState() != ServerState.END){
						if(target.getPlayer() < target.getMap().getSize() /* Oder wenn Spieler einen Rang besitzt*/){
							target.setPlayer(target.getPlayer() + 1);
							Data.getMinecraftServer().set(Data.getMinecraftServer().indexOf(target), target);
							Data.getBungeeCord().getListener().write("JOINSYSTEM TELEPORT " + cmd[2] + " " + cmd[3]);
						} else {
							listener.write("JOINSYSTEM ERROR " + cmd[2] + " FULL");
						}
					} else {
						listener.write("JOINSYSTEM ERROR " + cmd[2] + " OFFLINE");
					}
					
				} else {
					listener.write("JOINSYSTEM ERROR " + cmd[2] + " OFFLINE");
				}
				break;
			case "ERROR":
				target = null;
				for(MinecraftServer server : Data.getMinecraftServer()){
					if(server.getName().equals(cmd[4])){
						target = server;
					}
				}
				if(target != null){
					target.setPlayer(target.getPlayer() - 1);
					Data.getMinecraftServer().set(Data.getMinecraftServer().indexOf(target), target);
					target.getCListener().write("JOINSYSTEM ERROR " + cmd[2] + " " + cmd[3]);
					System.out.println("hey");
				}
				break;
			case "CONNECTED":
				target = null;
				for(MinecraftServer server : Data.getMinecraftServer()){
					if(server.getName().equals(cmd[3])){
						target = server;
					}
				}
				if(target != null){
					//Server von dem er kam
					target.setPlayer(target.getPlayer() - 1);
					Data.getMinecraftServer().set(Data.getMinecraftServer().indexOf(target), target);
					target.getCListener().write("JOINSYSTEM CONNECTED " + cmd[2]);
				}
				break;
			}
			
			break;
			
			
			
			
			
		//Build
		case "CLIENT":
			switch(cmd[1].toUpperCase()){
			case "REGISTER":
				if(cmd.length == 4){
					//Registriert neue Client
					//Command = CLIENT REGISTER
					//Parameter = [2]: ClientID; [3]: Ram
					
					ClientServer server = new ClientServer(Integer.parseInt(cmd[2]), 0, Integer.parseInt(cmd[3]), listener);
					Data.getClients().add(server);
					System.out.println("[CloudConnector] Client Registriert (ID:" + cmd[2] + "; Ram:" + cmd[3] + ")");
				}
				break;
			case "RELOAD":
				if(cmd.length == 4){
					//Reloadet Informationen eines Client
					//Command = CLIENT RELOAD
					//Parameter = [2]: ClientID; [3]: Neuer Ram
					
					for(ClientServer c : Data.getClients()){
						if(c.getID() == Integer.parseInt(cmd[2]) && c.getCListener() == listener){
							int old = c.getRam();
							c.setRam(Integer.parseInt(cmd[3]));
							Data.getClients().save(c);
							System.out.println("[CoudConnector] Client reloadet (ID:" + cmd[2] + "; Alter/Neuer Ram:" + old + "/" + cmd[3] + ")");
						}
					}
				}
				break;
			case "UNREGISTER":
				if(cmd.length == 3){
					//Unregistriert einen Client
					//Command = CLIENT UNREGISTER
					//Parameter = [2]: ClientID
					
					for(ClientServer c : Data.getClients()){
						if(c.getID() == Integer.parseInt(cmd[2]) && c.getCListener() == listener){
							Data.getClients().remove(c);
							System.out.println("[CoudConnector] Client unregistriert (ID:" + cmd[2] + ")");
						}
					}
				}
				break;
			}
			
			break;
		}
	}
	
	/*
	 * 			if(cmd[1].equals("UPDATE")){
					MinecraftServer target;
					switch(cmd[2]){
					case "SIZE":
						if(cmd.length == 5){
							target = null;
							for(MinecraftServer server : Data.getMinecraftServer()){
								if(server.getName().equals(cmd[3])){
									target = server;
								}
							}
							
							if(target != null){
								target.setPlayer(Integer.parseInt(cmd[4]));
								Data.getMinecraftServer().set(Data.getMinecraftServer().indexOf(target), target);
								for(MinecraftServer all : Data.getMinecraftServer()){
									if(all instanceof LobbyServer){
										if(! all.getServerState().equals(ServerState.BUILD)){
											all.getCListener().write("LOBBYSYSTEM UPDATE SIZE " + target.getName() + " " + target.getPlayer());
										}
									}
								}
							}
						}
						break;
					case "SERVERSTATE":
						break;
					}
	 * 
	 */
}

package Cloud;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Iterator;

import Client.ClientServer;
import Server.LobbyServer;
import Server.MinecraftServer;
import Server.ServerState;

public class CListener {

	private Socket s;
	private BufferedReader br;
	private PrintWriter pw;
	
	
	public CListener(Socket s) {
		this.s = s;
		try {
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			pw = new PrintWriter(s.getOutputStream());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void listen() {
		Thread acceptorThread = new Thread(new Runnable() {
			@Override
			public void run() {
				String data = null;
				boolean b = true;
				while(b){
					try {
						data = br.readLine();
					} catch (IOException e) {
						closeServer(CListener.this);
						b = false;
						System.out.println("Die Verbindung wurde geschlossen.");
					}
					
					if(data != null){
						Command.executeCommand(data, CListener.this);
					} else {
						closeServer(CListener.this);
						b = false;
					}
					
				}
			}
		});
		acceptorThread.start();
	}
	
	public void write(String s) {
		System.out.println(s);
		pw.println(s);
		pw.flush();
	}

	public void closeServer(CListener listener) {
		try {
			s.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		Iterator<MinecraftServer> i = Data.getMinecraftServer().iterator();
		while(i.hasNext()){
			MinecraftServer ms = i.next();
			if(ms.getCListener() == listener){
				//DataClass.server.remove(ms);
				
				//Server  L�schen (CLIENT)
				for(ClientServer server: Data.getClients()){
					server.getCListener().write("DELETE " + ms.getName());
				}
				
				//JoinSystem
				if(! (ms instanceof LobbyServer)){
					for(MinecraftServer all : Data.getMinecraftServer()){
						if(all instanceof LobbyServer){
							if(! all.getServerState().equals(ServerState.BUILD)){
								all.getCListener().write("LOBBYSYSTEM UNREGISTER " + ms.getName());
							}
						}
					}
				}
			}
		}
	}
	
	public void closeClient(CListener listener) {
		try {
			s.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Iterator<ClientServer> i = Data.getClients().iterator();
		while(i.hasNext()){
			ClientServer c = i.next();
			if(c.getCListener() == listener){
				Data.getClients().remove(c);
			}
		}
	}
}

package Cloud;

import java.util.ArrayList;
import java.util.HashMap;

import Client.ClientList;
import Client.ClientServer;
import Server.BungeecordServer;
import Server.LasertagServer;
import Server.LobbyServer;
import Server.Maps;
import Server.MinecraftServer;
import Server.ServerState;
import Server.ServerType;

public class Manager {
	
	private boolean managing = true;

	public Manager() {
		System.out.println("[CloudSystem] Manager wird erstellt");
	}
	
	public void startManager(Manager m){
		System.out.println("[CloudSystem] Manager wird gestartet ...");
		Thread manager = new Thread(new Runnable(){

			@Override
			public void run() {
				while(managing){
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					if(Data.getClients().size() != 0){
						
						if(Data.getBungeeCord() == null){
							//Configuration File
							int ram = 8;
							//
							CListener listener = m.choseClient(ram);
							if(listener != null){
								ClientServer target = Data.getClients().search(listener);
								if(target != null){
									target.setUsedRam(target.getUsedRam() + ram);
									Data.getClients().save(target);
									target.getCListener().write("SERVER CREATE BUNGEECORD " + ram);
									Data.setBungeeCord(new BungeecordServer(1, ServerState.BUILD, null));
								}
							}
							
						} else if(Data.getBungeeCord().getServerState().equals(ServerState.RUN)){	
							m.checkServer();
							
							for(ServerType type : ServerType.values()){

								
								System.out.println(type + " hat " + Data.getMaxPlayerInServer().get(type) + " Pl�tze");
								//System.out.println(type + " hat aktuell " + Data.get);
								
								int free = Data.getMaxPlayerInServerLobbys().get(type) - Data.getPlayerInServerLobbys().get(type);
								int befree = 0;
								
								if(type.equals(ServerType.LOBBY)){
									befree = (int) (Data.getOnlinePlayer() * 0.20);
									befree += 50;
								} else {
									//1. Spieler - Spieler auf der Lobby = Spieler in Games
									//2. Spieler im jeweiligem Spiel / Spieler in Games = Prozentsatz der Spieler die dieses Spiel gerade Spielen
									//3. Spieler auf LobbyServer * ProzentSatz
									befree =  (int) (Data.getPlayerInServer().get(ServerType.LOBBY) * (((double) Data.getPlayerInServer().get(type)) / ((double) (Data.getOnlinePlayer() - Data.getPlayerInServer().get(ServerType.LOBBY)))));
									befree += 40;
								}
								
								boolean b = true;
								while(b){
									if(free >= befree){
										b = false;
									}
									
									//TODO: Map auswahl
									Maps map = null;
									if(type.equals(ServerType.LOBBY)){
										map = Maps.LOBBY_MAP;
									} else if(type.equals(ServerType.LASERTAG)){
										map = Maps.LASERTAG_STANDART;
									}
									//TODO: Map auswahl
									if(map != null){
										CListener listener = m.choseClient(map.getRam());
										if(listener != null){
											ClientServer c = Data.getClients().search(listener);
											if(c != null){
												c.setUsedRam(c.getUsedRam() + map.getRam());
												Data.getClients().save(c);
												
												int id = selectID();
												listener.write("SERVER CREATE " + type.toString() + " " + map.getRam() + " " + map.toString() + " " + id);
												MinecraftServer server = null;
												if(type.equals(ServerType.LOBBY)){
													server = new LobbyServer(id, ServerState.BUILD, map);
												} else if(type.equals(ServerType.LASERTAG)){
													server = new LasertagServer(id, ServerState.BUILD, map);
												}
												Data.getMinecraftServer().add(server);
												free += map.getSize();
												
											} else {
												b = false;
											}
										} else {
											b = false;
										}
									} else {
										b = false;
									}
									
								}
							}
						}
						
						
					}
				}
				
			}
		});
		manager.start();
		System.out.println("[CloudSystem] Manager wurde erfolgreich gestartet");
	}



	private CListener choseClient(int ram) {
		CListener target = null;
		int maxRam = 0;
		
		for(ClientServer cs : Data.getClients()){
			if(maxRam < (cs.getRam() - cs.getUsedRam())){
				maxRam = cs.getRam() - cs.getUsedRam();
				target = cs.getCListener();
			}
		}
		
		if(maxRam < ram){
			target = null;
		}
		
		return target;
	}

	private Integer selectID() {
		int id = 0;
		boolean b = true;
		ArrayList<Integer> use = new ArrayList<Integer>();
		for(MinecraftServer ms : Data.getMinecraftServer()){
			use.add(ms.getID());
		}
		while(b){
			id ++;
			if(! use.contains(id)){
				b = false;
			}
		}
		return id;
	}
	
	public void checkServer(){
		HashMap<ServerType, Integer> maxPlayerInServer = new HashMap<ServerType, Integer>();
		HashMap<ServerType, Integer> playerInServer = new HashMap<ServerType, Integer>();
		HashMap<ServerType, Integer> maxPlayerInServerLobbys = new HashMap<ServerType, Integer>();
		HashMap<ServerType, Integer> playerInServerLobbys = new HashMap<ServerType, Integer>();
		int onlinePlayer = 0;
		//init HashMap
		for(ServerType type : ServerType.values()){
			maxPlayerInServer.put(type, 0);
			playerInServer.put(type, 0);
			maxPlayerInServerLobbys.put(type, 0);
			playerInServerLobbys.put(type, 0);
		}
		
		for(MinecraftServer s : Data.getMinecraftServer()){
			int player = s.getPlayer();
			int maxPlayer = s.getMap().getSize();
			
			onlinePlayer += player;
			
			ServerType type = null;
			if(s instanceof LobbyServer){
				type = ServerType.LOBBY;
				
			} else if(s instanceof LasertagServer){
				type = ServerType.LASERTAG;
			}
			
			if(type != null){
				maxPlayerInServer.put(type, playerInServer.get(type) + maxPlayer);
				playerInServer.put(type, playerInServer.get(type) + player);
				
				if(s.getServerState().equals(ServerState.LOBBY) || s.getServerState().equals(ServerState.RUN) || s.getServerState().equals(ServerState.BUILD)){
					maxPlayerInServerLobbys.put(type, maxPlayerInServerLobbys.get(type) + maxPlayer);
					playerInServerLobbys.put(type, playerInServer.get(type) + player);
				}
			}
		}
		
		Data.setOnlinePlayer(onlinePlayer);
		Data.getMaxPlayerInServer().putAll(maxPlayerInServer);
		Data.getPlayerInServer().putAll(playerInServer);
		Data.getMaxPlayerInServerLobbys().putAll(maxPlayerInServerLobbys);
		Data.getPlayerInServerLobbys().putAll(playerInServerLobbys);
	}
}

package Cloud;

import java.io.IOException;
import java.net.ServerSocket;

public class CloudSystem {
	
	private Manager manager;
	
	private ServerSocket ss;
	private Settings settings;
	private Acceptor acceptor;
	public CloudSystem(){
		
	}
	
	public void start(){
		
		settings = new Settings(this);
		
		try {
			ss = new ServerSocket(settings.port);
		} catch (IOException e) {
			System.out.println("[CloudSystem] Server Port ( " + settings.port + " ) wird bereits genutzt");
			e.printStackTrace();
			this.shutdown();
		}
		System.out.println("[CloudSystem] Server Port " + settings.port);
		
		acceptor = new Acceptor(ss);
		acceptor.start();
		
		manager = new Manager();
		manager.startManager(manager);
		
		
	}
	
	public void reload(){
		
	}
	
	public void shutdown(){
		System.out.println("[CloudSystem] System wird gestoppt ...");
		System.exit(0);
	}
	
	
	
	public Settings getSettings(){
		return settings;
	}
	
	public Manager getManager(){
		return manager;
	}
	
	public ServerSocket getServerSocket(){
		return ss;
	}

}

package Client;

import java.util.ArrayList;
import java.util.Iterator;

import Cloud.CListener;

public class ClientList extends ArrayList<ClientServer>{
	
	public ClientList(){
		super();
	}
	
	public ClientServer search(CListener listener){
		for(ClientServer c : this){
			if(c.getCListener() == listener){
				return c;
			}
		}
		return null;
	}
	
	public void save(ClientServer client){
		int i = this.indexOf(client);
		if(i != -1){
			this.set(i, client);	
		}
	}
}

package Client;

import Cloud.CListener;
import Cloud.Data;

public class ClientServer {
	
	private int id;
	private int usedram;
	private int ram;
	private CListener listener;
	
	public ClientServer(int id, int usedram, int ram, CListener listener){
		this.id = id;
		this.usedram = usedram;
		this.ram = ram;
		this.listener = listener;
	}
	
	public int getID() {
		return this.id;
	}
	
	public int getUsedRam() {
		return this.usedram;
	}

	public int getRam() {
		return this.ram;
	}

	public CListener getCListener() {
		return this.listener;
	}

	
	public void setID(int id) {
		this.id = id;
	}
	
	public void setUsedRam(int usedram) {
		this.usedram = usedram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public void setCListener(CListener listener) {
		this.listener = listener;
	}
}

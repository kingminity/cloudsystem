package Server;

import Cloud.CListener;

public class BungeecordServer {
	
	int id;
	ServerState state;
	CListener listener;
	
	public BungeecordServer(int id, ServerState state, CListener listener){
		this.id = id;
		this.state = state;
		this.listener = listener;
		
	}
	
	public int getID(){
		return this.id;
	}
	
	public ServerState getServerState(){
		return this.state;
	}
	
	public CListener getListener(){
		return this.listener;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public void setServerState(ServerState state){
		this.state = state;
	}
	
	public void setListener(CListener listener){
		this.listener = listener;
	}
}

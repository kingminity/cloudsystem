package Server;

import Cloud.CListener;

public class MinecraftServer {
	int id;
	ServerState state;
	Maps map;

	String ip;
	int port;
	String name;
	CListener listener;
	int player;

	public MinecraftServer(int id, ServerState state, Maps map) {
		this.id = id;
		this.state = state;
		this.map = map;
	}

	public void init(String ip, int port, String name, CListener listener, Integer player) {
		this.ip = ip;
		this.port = port;
		this.name = name;
		this.listener = listener;
		this.player = player;
	}

	// Getter
	public int getID() {
		return this.id;
	}

	public ServerState getServerState() {
		return this.state;
	}

	public Maps getMap() {
		return this.map;
	}

	public String getIP() {
		return this.ip;
	}

	public int getPort() {
		return this.port;
	}

	public String getName() {
		return this.name;
	}

	public CListener getCListener() {
		return this.listener;
	}
	
	public Integer getPlayer(){
		return this.player;
	}

	// Setter
	public void setID(int id) {
		this.id = id;
	}

	public void setServerState(ServerState state) {
		this.state = state;
	}

	public void setMap(Maps map) {
		this.map = map;
	}

	public void setIP(String ip) {
		this.ip = ip;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCListener(CListener listener) {
		this.listener = listener;
	}
	
	public void setPlayer(int player){
		this.player = player;
	}
	
	
}

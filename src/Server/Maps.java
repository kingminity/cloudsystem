package Server;

public enum Maps {
	
	LASERTAG_STANDART("Standart", 20, 1024),
	LOBBY_MAP("Map", 50, 1024);
	
	int size;
	String name;
	int ram;
	private Maps(String name, Integer size, int ram){
		this.size = size;
		this.name = name;
		this.ram = ram;
	}
	
	public int getSize(){
		return this.size;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getRam(){
		return this.ram;
	}

}
